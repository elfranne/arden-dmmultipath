type Dmmultipath::PathGroupingPolicy = Enum[
  'failover',
  'multibus',
  'group_by_serial',
  'group_by_prio',
  'group_by_node_name',
]
