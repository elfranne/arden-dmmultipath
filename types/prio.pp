type Dmmultipath::Prio = Enum[
  'const',
  'emc',
  'alua',
  'ontap',
  'rdac',
  'hp_sw',
  'hds',
  'random',
  'weightedpath',
]
