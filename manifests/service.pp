# Private class which manages services and other post-config activity.
#
# @summary Enable the multipathd service and regenerate initramfs (maybe)
class dmmultipath::service {
  # Make sure the service is running and configured to start at boot
  service { 'multipathd':
    ensure => 'running',
    enable => true,
  }

  # If this machine is SAN-boot we need to regenerate dracut, but only if we
  # didn't already... 
  if $dmmultipath::regen_initramfs {
    $cmd_dracut = 'dracut --force --add multipath --include /etc/multipath'
    exec { 'regenerate_initramfs_dmmultipath':
      command     => $cmd_dracut,
      path        => '/sbin:/bin:/usr/sbin:/usr/bin',
      refreshonly => true,
    }
  }
}
