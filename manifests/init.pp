# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @param find_multipaths [Boolean]
#   Sets the default parameter 'find_multipaths' to yes or no.
#
# @param user_friendly_names [Boolean]
#   Sets the default parameter 'user_friendly_names' to yes or no. This controls
#   whether device alias names like 'mpatha' are automatically created.
#
# @param udev_rules [Boolean]
#   When enabled any udev rules relevant to enabled device types will be
#   configured for this system.
#
# @param regen_initramfs [Boolean]
#   If enabled the initramfs will be regenerated for this machine. Note that
#   this can be crucial for SAN boot systems as they need multipathing to be up
#   and running when the system comes online.
#
# @param config_dir [Stdlib::Absolutepath]
#   Location of the multipath config files which will be read in and combined
#   with the base '/etc/multipath.conf'. Typically this is
#   '/etc/multipath/conf.d'.
#
# @param udev_rules_dir [Stdlib::Absolutepath]
#   Location where additional udev rules should be defined. Typically this is
#   '/etc/udev/rules.d'.
#
# @param enabled_devices [Array[Dmmultipath::SupportedModels]]
#   List of device types to enable on this system. Currently only ibm_2145 is
#   supported by this module.
#
# @param dev_data [Hash[Dmmultipath::SupportedModels,Dmmultipath::Device]]
#   This hash contains pre-configured lists of multipath 'device' directives.
#   If a given device is enabled via $enabled_devices the corresponding values
#   present in this hash will be used to create a file in the multipath config
#   directory.
#
# @param dev_udev_data [Hash[Dmmultipath::SupportedModels,Dmmultipath::DeviceUdev]]
#   Contains udev parameters for the provided device type. Note that this
#   currently only supports SCSI timeouts. If udev_rules is enabled rules
#   corresponding to the devices in $enabled_devices will be generated.
#
# @param packages [Array[String]]
#   List of packages which should be installed for proper multipathing
#   functionality.
#
# @example Default config enabling storwize
#   class { 'dmmultipath':
#     enabled_devices => ['ibm_2145'],
#   }
#
# @example Storwize with all options enabled
#   class { 'dmmultipath':
#     find_multipaths     => true,
#     user_friendly_names => true,
#     udev_rules          => true,
#     regen_initramfs     => true,
#     config_dir          => '/etc/multipath/conf.d',
#     udev_rules_dir      => '/etc/udev/rules.d',
#     enabled_devices     => ['ibm_2145'],
#   }
#
class dmmultipath (
  Boolean $find_multipaths                                                  = false,
  Boolean $user_friendly_names                                              = true,
  Boolean $udev_rules                                                       = true,
  Boolean $regen_initramfs                                                  = false,
  Stdlib::Absolutepath $config_dir                                          = '/etc/multipath/conf.d',
  Stdlib::Absolutepath $udev_rules_dir                                      = '/etc/udev/rules.d',
  Array[Dmmultipath::SupportedModels] $enabled_devices                      = undef,
  Hash[Dmmultipath::SupportedModels,Dmmultipath::Device] $dev_data          = undef,
  Hash[Dmmultipath::SupportedModels,Dmmultipath::DeviceUdev] $dev_udev_data = undef,
  Array[String] $packages                                                   = undef,
){
  contain dmmultipath::install
  contain dmmultipath::configure
  contain dmmultipath::service

  Class['dmmultipath::install']
  -> Class['dmmultipath::configure']
  ~> Class['dmmultipath::service']
}
