# A simple internal class which manages all install-time steps
#
# @summary Install any necessary packages for DM Multipath
class dmmultipath::install {

  # Install each of the requried packages for this distro
  $dmmultipath::packages.each | $package | {
    package { $package:
      ensure => present,
    }
  }
}
