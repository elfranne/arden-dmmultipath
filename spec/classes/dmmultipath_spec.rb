require 'spec_helper'

describe 'dmmultipath' do
  let(:params) do
    {
      enabled_devices: ['ibm_2145'],
    }
  end

  # Ensure compliation works with default parameters for each OS
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile.with_all_deps }

      it 'contains the correct number of files' do
        is_expected.to have_file_resource_count(5)
      end

      it 'creates the standard directory structure' do
        is_expected.to contain_file('/etc/multipath/').with(
          ensure: 'directory',
          owner: 'root',
          group: 'root',
          mode: '0755',
        )
        is_expected.to contain_file('/etc/multipath/conf.d/').with(
          ensure: 'directory',
          owner: 'root',
          group: 'root',
          mode: '0755',
        )
      end

      it 'creates the multipath config file' do
        is_expected.to contain_file('/etc/multipath.conf').with(
          ensure: 'file',
          owner: 'root',
          group: 'root',
          mode: '0664',
        )
        is_expected.to contain_file('/etc/multipath.conf').with_content(
          %r{user_friendly_names yes},
        )
        is_expected.to contain_file('/etc/multipath.conf').with_content(
          %r{find_multipaths no},
        )
        is_expected.to contain_file('/etc/multipath.conf').with_content(
          %r{config_dir /etc/multipath/conf.d},
        )
      end

      it 'creates the storwize device file with the correct content' do
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with(
          ensure: 'file',
          owner: 'root',
          group: 'root',
          mode: '0644',
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{vendor ["]IBM["]},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{product ["]\^2145["]},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{dev_loss_tmo "120"},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{failback "immediate"},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{features "1 queue_if_no_path"},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{hardware_handler "0"},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{path_checker "tur"},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{path_grouping_policy "group_by_prio"},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{prio "alua"},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{rr_min_io_rq "1"},
        )
        is_expected.to contain_file('/etc/multipath/conf.d/ibm_2145.conf').with_content(
          %r{rr_weight "uniform"},
        )
      end

      it 'creates a udev rules file for storwize SCSI timeout' do
        is_expected.to contain_file('/etc/udev/rules.d/99-ibm-2145.rules').with(
          ensure: 'file',
          owner: 'root',
          group: 'root',
          mode: '0644',
        )
        is_expected.to contain_file('/etc/udev/rules.d/99-ibm-2145.rules').with_content(
          %r!SUBSYSTEM==["]block["], ACTION==["]add["], ENV{ID_VENDOR}==["]IBM["],ENV{ID_MODEL}==["]2145["], RUN[+]=["]/bin/sh -c 'echo 120 >/sys/block/[%]k/device/timeout'["]!,
        )
      end

      it 'correctly manages the multipath service' do
        is_expected.to contain_service('multipathd').with(
          ensure: 'running',
          enable: 'true',
        )
      end

      context 'in a SAN boot environment' do
        before(:each) do
          params.merge!(
            regen_initramfs: true,
          )
        end

        it 'regenerates the initramfs' do
          is_expected.to contain_exec('regenerate_initramfs_dmmultipath').with(
            command: 'dracut --force --add multipath --include /etc/multipath',
            path: '/sbin:/bin:/usr/sbin:/usr/bin',
            refreshonly: true,
          )
        end
      end
    end
  end

  context 'on RedHat platforms' do
    let(:facts) do
      {
        os: {
          family: 'RedHat',
          release: {
            major: '7',
          },
        },
      }
    end

    it 'installed the multipath packages' do
      is_expected.to contain_package('device-mapper-multipath').with(
        ensure: 'present',
      )
    end
  end
end
